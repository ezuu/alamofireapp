//
//  NetworkingService.swift
//  alamofireapp
//
//  Created by Ezaden Seraj on 08/12/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation
import Alamofire

typealias JSON = [String: Any]
class NetworkingService {
    
    static let shared = NetworkingService()
    private init() {
        
    }
    
    func getPeople(success successBlock: @escaping (GetPeopleResponse)-> Void) {
        Alamofire.request("https://swapi.co/api/people").responseJSON { (response) in
            guard let json = response.result.value as? JSON else {return}
            
            do {
                let getPeopleRespone = try GetPeopleResponse(json: json)
                successBlock(getPeopleRespone)
            } catch {
                
            }
        }
        
    }
    
}
