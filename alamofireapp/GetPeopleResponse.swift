//
//  GetPeopleResponse.swift
//  alamofireapp
//
//  Created by Ezaden Seraj on 08/12/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation

struct GetPeopleResponse {
    
    let people : [Person]
    
    init(json: JSON) throws {
        guard let results = json["results"] as? [JSON] else {throw NetworkingError.networkingError }
        let people = results.map{ Person(json: $0)}.flatMap{$0}
        self.people = people
    }
}
