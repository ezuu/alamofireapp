//
//  NetworkingError.swift
//  alamofireapp
//
//  Created by Ezaden Seraj on 08/12/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    case networkingError
}
