//
//  ViewController.swift
//  alamofireapp
//
//  Created by Ezaden Seraj on 08/12/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

class HomeController: UITableViewController {

    let cellId = "cellId"
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "GET", style: .done, target: self, action: #selector(handleGet))
        navigationItem.title = "People"
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         navigationController?.navigationBar.barTintColor = UIColor.purple
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        cell.textLabel?.text = people[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    @objc func handleGet() {
        NetworkingService.shared.getPeople { response in
            self.people = response.people
            self.tableView.reloadData()
        }
    }
    

}

